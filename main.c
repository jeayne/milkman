#if __STDC_VERSION__ < 199901L
#error "ATTENTION: C99 mandatory. Compile with -std=c99 or -std=c11"
#endif

#include <inttypes.h>
#include <ctype.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
Compilation
  gcc -std=c99 milkman.c -o milkman
Usage
  milkman -h
*/

/**We need 3 data structures : **/

/* 1/ A state: It defined the contains of each bottle.
   Because the number of bottles is limited to 5 we can
   use an union with a 64 bits and a array of 8 bytes.
   The 64 bits make states comparison easy and fast.
   The array allows to manipulate each bottle individually.
*/

typedef union {
    uint64_t global;
    uint8_t  bottle[8];
} State_t;

int     nb;          // number of bottles
State_t volume;      // volume of each bottle

/* 2/ A path: It defines the list of manipulation to apply to reach a state
   It is just a string of char pairs.
   Example: "ABACBA" means: pour A into B,
                       then pour A into C,
                       then pour B into A.

    A question remains: What is the longest path ?
    It depends on - number of bottles
                  - volume of the bottles
                  - initial filling of the bottles
 */

char currentPath[10000*2 + 1]; // List of moves giving the current state
size_t maxlen;                 // To track the length of currentPath

/* 3/ A chained list of positions. A position is a state and the path leading to this state.
   This list had two goals: - avoid cycles
                            - keep shortest path to reach each possible state
*/

typedef struct position {
    State_t state;
    char*  path;
    struct position* next;
} Position_t;

Position_t* positions;


/*-------------------------+
 | Display function        |
 | usage, error and trace  |
 +-------------------------*/

static
void usage(){
    fprintf(stderr,
        "\nUSAGE\n"
        "  milkman [options] [volume [filling]]\n"
        "\n"
        "  options:\n"
        "    -h -H : Help. Display this message.\n"
        "    -t -T : Trace. Activate mode trace mode.\n"
        "\n"
        "  volume:\n"
        "    -A vol -B vol -C vol -D vol -E vol\n"
        "    These 5 upper case options define the volume of the bottles.\n"
        "    -A 12 -B 8 -C 5 means we use three bottles having respectively\n"
        "    a volume of 12, 8 and 5 liters (gallons, or cubic meters...).\n"
        "\n"
        "  filling:\n"
        "    -a val -b val -c val -d val -e val\n"
        "\n"
        "    By default all bottles are empty excepted 'A' which is full."
        "    But with the lower case options you can define the initial\n"
        "    filling for each bottle.\n"
        "    -a 10 -b 3 -c 0 means the bottles A, B et C are respectively"
        "    filled with 10, 3 and 0 liters (gallons, cubic meters...)\n"
        "\n"
        "  Notes:\n"
        "    * The volume of a bottle cannot exceed 255 liters (gallons, ...).\n"
        "    * The upper case options must be ordered my name and volume without missing."
        "        -A 12 -C 8 -B 5  <== not ordered by name.\n"
        "        -A 12 -B 8 -D 5  <== -C is missing.\n"
        "    * You can only set the filling of defined bottles."
        "        -A 12 -B 8 -c 8  <== the bottle C is not defined.\n"
        "    * Le filling cannot exceed the volume.\n"
        "        -A 12 -a 15      <== 15 liters is too much for a 12 liters bottle.\n"
        "    * Without any information the default setup is '-A 12 -B 8 -C 5 -a 12'\n"
        "\n"
    );
}

static
void fatal(){
    perror("FATAL ERROR");
    exit(3);
}

static
void error(const char* format, ...){
    fprintf(stderr, "\nERROR\n  ");
    va_list args;
    va_start (args, format);
    vfprintf(stderr, format, args);
    va_end (args);
    usage();
    exit(EXIT_FAILURE);
}

int trace_is_on = 0;

static
void trace(const char* format, ...){
    if (!trace_is_on) return;
    va_list args;
    va_start (args, format);
    vfprintf(stderr, format, args);
    va_end (args);
}


#ifdef __STRICT_ANSI__
// strdup() is not defined with ANSI
static char* strdup (const char *s) {
    char* const d = malloc(strlen(s) + 1);
    if (d==NULL) fatal();
    return strcpy(d,s);
}
#endif // __STRICT_ANSI__


/*-------------------------+
 | Core functions          |
 +-------------------------*/

// Determines is a state is in the list of the know positions.
// Returns the path it the state exists, else returns null.
static
char* isKnown(const State_t state){
    const Position_t *pos = positions;
    while(pos!=NULL){
        if (state.global == pos->state.global)
            return pos->path;
        pos = pos->next;
    }
    return NULL;
}

// Adds a state ant its path to the list of the know positions.
static
void addPosition(const State_t state, const char* const path){
    Position_t* head = positions;
    positions = calloc(1,sizeof(Position_t));
    if (positions==NULL) fatal();
    positions->state   = state;
    positions->path = strdup(path);
    positions->next   = head;
}

// Highly recursive function analyzing all possible moves from a state
// 'len' is the length of  the 'currentPath' leading to 'state'
static
void analyser(const State_t state, size_t len){

    for (int src=0; src<nb; src++){
        if (state.bottle[src]==0) continue; // la bottle source est vide

        for (int dst=0; dst<nb; dst++){
            if (dst==src) continue;     // bottles source et destination sont identiques
            const int dstLibre = volume.bottle[dst] - state.bottle[dst];
            if (dstLibre==0) continue;  // la bottle destination est deja pleine

            // execute the move: pour src into dst
            State_t resultat;
            resultat.global = state.global;
            const int transfert = (state.bottle[src]<dstLibre)? state.bottle[src] : dstLibre;
            resultat.bottle[src] -= transfert;
            resultat.bottle[dst] += transfert;

            if( (len+2) >= sizeof(currentPath)) fatal();

            currentPath[len]   = 'A' + src;
            currentPath[++len] = 'A' + dst;
            currentPath[++len] = 0;

            if (len>maxlen) maxlen=len; // just for statistics

            const char* const knownPath = isKnown(resultat);
            if (knownPath){
                // this state is already know and we have the path to reach this path
                 const size_t lenKnown = strlen(knownPath);
                if (len<lenKnown){
                    // we found a sorter path for the same state.
                    // we have to parse all know paths starting with knowPath
                    // and substitute it by currentPath
                    //    currentPath == xxxxxx
                    //      knownPath == aaaaaaaaaaaa
                    //     pos.path   == aaaaaaaaaaaaoooooo
                    //                => xxxxxxoooooo
                    trace("> %016" PRIX64 " %s remplace %s\n", resultat.global, currentPath, knownPath);
                    const Position_t* pos = positions;
                    while(pos!=NULL){
                        if (strstr(pos->path, knownPath) == pos->path){
                            trace(". %016" PRIX64 " %s", pos->state.global, pos->path);
                            strcpy(pos->path, currentPath);
                            strcat(pos->path, pos->path+lenKnown); // tricky
                            trace(" -> %s\n", pos->path);
                        }
                        pos = pos->next;
                    }
                    analyser(resultat, len);
                }else{
                    trace("  %016" PRIX64 " %s same state as %s\n", resultat.global, currentPath, knownPath);
                }
            }else{
                trace("+ %016" PRIX64 "%s\n", resultat.global, currentPath);
                addPosition(resultat, currentPath);
                analyser(resultat, len);
            }
            currentPath[len-=2] = 0;
        }
    }
}

/*---------------------------------+
 | main() does 4 things            |
 |  - analyses the command line    |
 |  - initializes                  |
 |  - starts the analysis          |
 |  - displays results             |
 +---------------------------------*/

int main(int argc, char **argv){

    volume.global  = 0;
    State_t state0 = {0};
    int c;
    while ((c = getopt(argc, argv, "A:B:C:D:E:a:b:c:d:e:HhTt")) != -1){
        switch(c){
        case 'A': case 'B': case 'C': case 'D': case 'E': {
            const int pos = c - 'A';
            if (pos!=nb)
                error("Definition of bottle volume must be done in alphabetical order: -A=xx -B=yy ...\n");
            const int val = atoi(optarg);
            if (val<0 || val>255)
                error("Volume of bottle %c must be in [0..255]\n", c);
            volume.bottle[pos] = val;
            nb++;
            break;
        }
        case 'a': case 'b': case 'c': case 'd': case 'e': {
            const int pos =  c - 'a';
            if (pos>=nb)
                error("The volume of bottle %c is not defined\n", 'A'+pos);
            const int maxi = volume.bottle[pos];
            const int val = atoi(optarg);
            if (val<0 || val>maxi)
                error("The initial filling of bottle %c must be in [0..%d]\n", 'A'+pos, maxi);
            state0.bottle[pos] = val;
            break;
        }

        case 'h': case 'H':
            usage();
            return 0;

        case 'T': case 't':
            trace_is_on = 1;
            break;

        case '?':
            if (strchr("ABCDEabcde", optopt))
                error("Option -%c requires a value\n", optopt);
            else if (isprint(optopt))
                error("Option -%c is unknown\n", optopt);
            else
                error("Option 0x%x is unknown\n", optopt);
            break;

        default:
            error("Invalid parameter.\n");
        }
    }

    if (optind < argc)
        error("Unknown parameter(s).\n");

    // Without any volume definition we setup 3 bottles
    if (volume.global==0){
        volume.bottle[0] = 12;
        volume.bottle[1] =  8;
        volume.bottle[2] =  5;
        nb = 3;
    }

     // Without any filling definition bottle A is filled to the maximum
    if (state0.global==0)
        state0.bottle[0] = volume.bottle[0];

    trace("\nTrace activated\n");
    trace("  %016" PRIX64 " Volumes\n",        volume.global);
    trace("  %016" PRIX64 " Starting state\n", state0.global);

    // Add this initial state and its path (emty) to the list of positions
    addPosition(state0, currentPath);

    // GO! Analyze this initial state
    analyser(state0, 0);

    trace("Longest path analyzed: %d moves\n", maxlen/2);

    // Dump the best path for each possible state
    printf("\n========== RESULTS pour these %d bottles: ", nb);
    for (int i=0; i<nb; i++)
        printf("%d ", volume.bottle[i]);
    puts("========\n");
    trace("......EEDDCCBBAA ");
    for (int i=0; i<nb; i++)
        printf("|   %c ", 'A'+i);
    puts("|moves| path ...");

    const Position_t* pos = positions;
    while(pos){

        // State
        const State_t state = pos->state;
        trace("%016" PRIX64 " ", state.global);
        for (int i=0; i<nb; i++)
            printf("| %3d ", state.bottle[i]);

        // Shorter path leading to this state
        const char* const path = pos->path;
        const int ncoups = strlen(path)/2;
        printf("|%4d |", ncoups);
        if (ncoups<20){
            int i = 0;
            while(path[i]){
                printf(" %c->%c", path[i], path[i+1]);
                i+=2;
            }
            puts("");
        }else{
            puts("  ...");
        }

        pos = pos->next;
    }

    puts("\nFin!");
    return 0;
}

/* For those who didn't seen the message at the beginning ... */
#if __STDC_VERSION__ < 199901L
#error "ATTENTION: C99 mandatory. Compile with -std=c99 ou -std=c11"
#endif
